import java.util.Scanner;

/**
 * Class Main is able to receive data, process it and show the results
 */
public class Main {
    /**
     * @param args
     */
    public static void main(String[] args) {
        int leftBorder = 0;
        int rightBorder = 0;
        int sumOfOdd = 0;
        int sumOfEven = 0;
        int maxOdd = 0;
        int maxEven = 0;
        int sizeOfArray = 0;
        double persOfOdd;
        double persOfEven;
        Scanner in;
        while (true) {
            System.out.println("Enter the interval");
            in = new Scanner(System.in);
            if (in.hasNextInt()) {
                leftBorder = in.nextInt();
                rightBorder = in.nextInt();
                break;
            } else {
                System.out.println("Wrong number!");
            }
        }
        System.out.println("Odd numbers:");
        for (int i = leftBorder; i < rightBorder; i++) {
            if (i % 2 != 0) {
                if (i > maxOdd) {
                    maxOdd = i;
                }
                System.out.print(i + " ");
                sumOfOdd += i;
            }

        }
        System.out.println(" ");
        System.out.println("Sum of odd numbers:" + sumOfOdd);
        System.out.println("Even numbers:");
        for (int i = rightBorder; i > leftBorder; i--) {
            if (i % 2 == 0) {
                if (i > maxEven) {
                    maxEven = i;
                }
                System.out.print(i + " ");
                sumOfEven += i;
            }
        }
        System.out.println(" ");
        System.out.println("Sum of even numbers:" + sumOfEven);
        while (true) {
            System.out.println("Enter the size of Fibonacci array:");
            in = new Scanner(System.in);
            if (in.hasNextInt()) {
                sizeOfArray = in.nextInt();
                break;
            } else {
                System.out.println("Wrong number!");
            }
        }
        Fibbonachi.Fibonacci(maxOdd, maxEven, sizeOfArray);
    }
}




