/**
 *
 */
public class Fibbonachi {
    /**
     * @param f1
     * @param f2
     * @param sizeOfArray
     */
    public static void Fibonacci(int f1, int f2, int sizeOfArray) {
        int fibb1 = f1;
        int fibb2 = f2;
        int fibb3 = f1 + f2;
        int countOfFibb = 0;
        int countOfOddFibb = 0;
        int countOfEvenFibb = 0;
        double persOfEven;
        double persOfOdd;
        System.out.println("Fibonacci numbers:");
        System.out.print(fibb1 + " " + fibb2 + " ");
        do {
            fibb3 = fibb1 + fibb2;
            fibb1 = fibb2;
            fibb2 = fibb3;
            System.out.print(fibb3 + " ");
            countOfFibb++;
            if (fibb3 % 2 == 0) {
                countOfEvenFibb++;
            } else {
                countOfOddFibb++;
            }
        } while (countOfFibb <= sizeOfArray - 3);
        FibonacciPercent(countOfFibb, countOfEvenFibb, countOfOddFibb);
    }

    /**
     *
     * @param countOfFib
     * @param countOfEven
     * @param countOfOdd
     */
    public static void FibonacciPercent(int countOfFib, int countOfEven, int countOfOdd) {
        double persOfEven;
        double persOfOdd;
        persOfEven = (double) (countOfEven * 100) / countOfFib;
        persOfOdd = (double) (countOfOdd * 100) / countOfFib;
        System.out.println(" ");
        System.out.format("Percentage of odd: %.3f percent", persOfOdd);
        System.out.println(" ");
        System.out.format("Percentage of even: %.3f percent", persOfEven);
    }
}

